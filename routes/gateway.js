const express = require('express');
const router = express.Router({});
const mongoose = require('mongoose');
var deepEqual = require('deep-equal');
let db = mongoose.connection;



/*error handling
  if db does not open, log in console*/
db.once('open', function callback() {
    console.log("Connected to database");
});

db.on('error', function(err){
  console.log("error, could not connect");
});

//mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/sweii', { useNewUrlParser: true });


const Schema = mongoose.Schema;

const heartbeatSchema = new Schema ({

  gatewayId: {type: String, required: true},
  timestamp: {type: String, required: true}
},{collection: 'gateway'});

const diagnosticSchema = new Schema ({
  diagnostic: {type:String, required: true}
}, {collection: 'diagnostic'});

const onDemandDiagnosticSchema = new Schema ({
  gatewayId: {type: String, required: true},
  diagnostic: {type: String, required: true}
}, {collection: 'onDemand2'});

const batterySchema = new Schema ({
percent: {type: String, required: true},
secsleft: {type: String, required: true},
power_plugged: {type: String, required: true}

},{collection: 'battery'});

const cpuSchema = new Schema ({
total: {type: String, required: true},
used: {type: String, required: true},
free: {type: String, required: true},
percent: {type: String, required: true}

},{collection: 'cpu'});

const Cpu = mongoose.model('Cpu', cpuSchema);

const Battery = mongoose.model('Battery', batterySchema);

const OnDemandDiagnostic = mongoose.model('OnDemandDiagnostic', onDemandDiagnosticSchema);

const Diagnostic = mongoose.model('Diagnostic', diagnosticSchema);

/*heartbeat model*/
const Heartbeat = mongoose.model('Heartbeat', heartbeatSchema);

/*heartbeat = {'gatewayId': String,
                'timestamp': String}; */
/*return a heartbeat model from the database*/
router.get('/getHeartbeat', function (req,res) {

  Heartbeat.find()
    .then(function(doc) {
      const size = doc.length - 1;
      const gatewayId = doc[size].gatewayId || [];
      const timestamp = doc[size].timestamp || [];

      const data = {
        "gatewayId": gatewayId,
        "timestamp": timestamp
      };
      console.log(data);
      res.send(data);
      // res.send({items: doc});
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

/*heartbeat = {'gatewayId': String,
                'timestamp': String}; */
/*insert a heartbeat model into the database*/
router.post('/insertHeartbeat', function (req,res) {

console.log(req);
const gatewayId = req.body.gatewayId;
console.log(gatewayId);
const timestamp = req.body.timestamp;
console.log(timestamp);

const data = {
  'gatewayId' : gatewayId,
  'timestamp': timestamp
  };

  console.log(data);

  var temp = [];
  OnDemandDiagnostic.find({
    gatewayId: gatewayId
  })
    .exec()
    .then(docs => {
      temp = docs;
      console.log("here");
      console.log(docs);

        console.log("here2")
        console.log(temp);
      const diagnostic = docs[0].diagnostic || [];


      res.status(201).json({
        'onDemand': diagnostic,
      });

    })
    .catch(err => {
      console.log(err);
      const heartbeat = new Heartbeat(data);
      heartbeat.save()
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });

      res.status(201).json(heartbeat);

    });

if (!temp==[]){
  OnDemandDiagnostic.remove({
  gatewayId: gatewayId
  })
        .exec()
        .then(result => {
            console.log("Diagnostics deleted");
            console.log(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
  };

});

router.post('/insertBattery', function (req,res) {
  const percent = req.body.percent;
  const secsleft = req.body.secsleft;
  const power_plugged = req.body.power_plugged;

  const data = {
    'percent': percent,
    'secsleft': secsleft,
    'power_plugged': power_plugged
  };

  console.log(data);

  const battery = new Battery(data);
  battery.save()
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });

  res.status(201).json(battery);

});

router.post('/insertCPU', function (req,res) {
  const total = req.body.total;
  const used = req.body.used;
  const free = req.body.free;
  const percent = req.body.percent;

  const data = {
    "total": total,
    "used": used,
    "free": free,
    "percent": percent
  };

  console.log(data);

  const cpu = new Cpu(data);
  cpu.save()
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });

  res.status(201).json(cpu);
});

router.get('/getBattery', function (req,res) {


  var temp = [];
  Battery.find()
    .then(function(doc) {
      temp = doc;
      const size = doc.length - 1;
      const percent = doc[size].percent || [];
      const secsleft = doc[size].secsleft || [];
      const power_plugged = doc[size].power_plugged || [];

      const data = {
        "perecent": percent,
        "secsleft": secsleft,
        "power_plugged": power_plugged
      };
      console.log(data);
      res.send(data);

  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.get('/getCPU', function (req,res) {

  Cpu.find()
    .then(function(doc) {
      const size = doc.length - 1;
      const total = doc[size].total || [];
      const used = doc[size].used || [];
      const free = doc[size].free || [];
      const percent = doc[size].percent || [];


      const data = {
        "size": size,
        "total": total,
        "used": used,
        "free": free,
        "perecent": percent
      };
      console.log(data);
      res.send(data);
      // res.send({items: doc});
  })
  .catch(err => {
    res.status(500).json({
      error: err
    });
  });
});


router.post('/getDiagnostic', function (req,res) {


  const scriptName = req.body.diagnostic;


//    formatting into JSON

   const diagnostic = {
    'diagnostic': scriptName
  };

  const data = new Diagnostic(diagnostic);
  data.save();

  res.status(201).json(diagnostic);

});



//input must be a onDemandDiagnostic
/*
  {  'gatewayId': gatewayId,
    'diagnostic': diagnostic}
*/
router.post('postOnDemandDiagnostic2', function(req,res){
  const id = req.body.gatewayId;
  const gateway = {'gatewayId': id};
  const diagnostic = req.body.diagnostic;
  const onDemand = new OnDemandDiagnostic({
    'gatewayId': id,
    'diagnostic': diagnostic
  });

  const array = [];

//begin authorization

    onDemand.save()
      .then(result => {
        res.status(201).json({
                'onDemandResult': result,
            });

      })
      .catch(err => {
      res.status(504).json({'error':err });
    });


  //end authorization

  //catch authorization failure
});

router.post('postOnDemandDiagnostic', function(req,res){
  const id = req.body.gatewayId;
  const gateway = {'gatewayId': id};
  const diagnostic = req.body.diagnostic;
  const onDemand = new OnDemandDiagnostic({
    'gatewayId': id,
    'diagnostic': diagnostic
  });

  const array = [];

//begin authorization

//onDemand.find()
    onDemand.find(gateway)
      .exec()
      .then(docs => {
        console.log(docs);
        array = docs;
      });
      console.log(array);

      res.status(201).json({'onDemand': array});




  //end authorization

  //catch authorization failure
});


router.post('/insertOnDemand', function (req,res) {

const gatewayId = req.body.gatewayId;
const diagnostic = req.body.diagnostic;

const data = {
  'gatewayId' : gatewayId,
  'diagnostic': diagnostic
  };

  console.log(data);

  const onDemandDiagnostic = new OnDemandDiagnostic(data);
  onDemandDiagnostic.save()
  .catch(err => {
    res.status(500).json({
      error: err
    });
  });

  res.status(201).json(onDemandDiagnostic);
  console.log("success");


});

router.post('/registerGateway', function (req,res) {
    res.json({
      'status': 'OK'
    });
    console.log(res);
});

router.post('/authorizeGateway', function (req,res){
    res.json({
      'status': 'OK'
    });
    console.log(res);
});

router.get('/usersAuthorized', function(req,res){
    res.json({
      'status': 'OK'
    });
    console.log(res);
});


router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = router;
