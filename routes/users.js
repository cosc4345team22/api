const express = require('express');
const router = express.Router({});
const mongoose = require('mongoose');
let db = mongoose.connection;

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/sweii', { useNewUrlParser: true });

const Schema = mongoose.Schema;

const userSchema = new Schema ({
username: {type: String, required: true},
password: {type: String, required: true}

},{collection: 'user'});

const User = mongoose.model('User', userSchema);

/* GET users listing. */
router.post('/insertUser', function (req, res) {

    var newUser = {
      username: req.body.username,
      password: req.body.password,
    }

    const user = new User(newUser);
    user.save();

    res.status(201).send(user);
});

router.get('/getUser:username', function (req, res) {
  const username = req.body.username;

  User.find({
    username: username
  })
    .exec()
    .then(docs => {


      const user = docs[0].user || [];



      res.status(201).json({'user': user});
    })
    .catch(err => {
      res.status(500).json({
        error:err
      });
    });
});


router.get('/register', function (req, res) {
    res.json({
        'status': 'OK'
    });
    console.log(res);
});

router.post('/', function (req, res) {
    res.status(201).json(req.body);
});

router.put('/', function (req, res) {
    res.status(202).send();
});

router.delete('/', function (req, res) {
    res.status(202).send();
    process.exit();
});

router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = router;
