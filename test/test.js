//the test the env variable is set to test
//Test
process.env.NODE_ENV = 'test';

var supertest = require("supertest");
let mongoose = require("mongoose");
// var Heartbeat = require('../models/heartbeatModel');


//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let app = require('../app');

const gatewayrouter = require('../routes/gateway');


const assert = require('assert');

function asyncFunction() {
    return new Promise(resolve => {
        setTimeout(resolve, 10);
    });
}


chai.use(chaiHttp);


  describe('/GET Heartbeat', () => {
      it('it should GET last heartbeats', async() =>   {
        chai.request(gatewayrouter)
            .get('/getHeartbeat')

            .end((err, res) => {
              res.should.have.status(200);
             done();
            });
      });
  });
    describe('/GET CPU', () => {
      it('it should GET the last on CPU entry', async() =>  {
        chai.request(gatewayrouter)

            .get('/getCPU')
            .end((err, res) => {
              res.should.have.status(200);
             done();
            });
      });
  });

  describe('/GET battery', () => {
    it('it should GET the last on Battery entry', async() =>  {
      chai.request(gatewayrouter)

          .get('/getBattery')
          .end((err, res) => {
            res.should.have.status(200);
           done();
          });
    });
});

describe('/GET diagnostic', () => {
  it('it should all diagnostic entries', async() =>  {
    chai.request(gatewayrouter)

        .get('/getDiagnostic')
        .end((err, res) => {
          res.should.have.status(200);
         done();
        });
  });
});

describe('/insert Diagnostic', () => {
  it('it should insert a Diagnostic object', async() =>  {
    chai.request(gatewayrouter)

        .get('/insertDiagnostic')
        .end((err, res) => {
          res.should.have.status(201);
         done();
        });
  });
});

describe('/insert onDemand', () => {
  it('it should insert an onDemand', async() =>  {
    chai.request(gatewayrouter)

        .get('/insertOnDemand')
        .end((err, res) => {
          res.should.have.status(201);
         done();
        });
  });
});

describe('/insert heartbeat', () => {
  it('it should insert an onDemand', async() =>  {
    chai.request(gatewayrouter)

        .get('/insertHeartbeat')
        .end((err, res) => {
          res.should.have.status(201);
         done();
        });
  });
});

describe('/insert Battery', () => {
  it('it should insert a battery object', async() =>  {
    chai.request(gatewayrouter)

        .get('/insertBattery')
        .end((err, res) => {
          res.should.have.status(201);
         done();
        });
  });
});

describe('/insert CPU', () => {
  it('it should insert a CPU', async() =>  {
    chai.request(gatewayrouter)

        .get('/insertCPU')
        .end((err, res) => {
          res.should.have.status(201);
         done();
        });
  });
});
