const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const assert = require ('assert');


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const gatewayRouter = require('./routes/gateway');


const app = express();
app.use(logger('dev'));
app.use(express.json());
//app.use(express.bodyParser());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false}));

app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/gateway', gatewayRouter);

module.exports = app;
